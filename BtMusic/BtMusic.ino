///////////////////////////////////////////////////////////////////////////////
// INCLUDES & DEFINES

#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"
#include "esp_a2dp_api.h"
#include "esp_avrc_api.h"
#include <driver/i2s.h>
#include <Preferences.h>
#include <driver/adc.h>
#include "esp_adc_cal.h"

#define LEDC_CHANNEL_0     0
#define LEDC_TIMER_13_BIT  13
#define LEDC_BASE_FREQ     3
#define LED_PIN            5

///////////////////////////////////////////////////////////////////////////////
// Variables

int iFreq = 2;
int iSteps = 8;
int iCurStep = 0;


Preferences prefs;

char cCurrentChar;
String sMessage;

i2s_port_t i2s_port = (i2s_port_t)0;
i2s_config_t i2s_config = {
	.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN),
	.sample_rate = 44100, 							// corrected by info from bluetooth
	.bits_per_sample = (i2s_bits_per_sample_t)16, 	//the DAC module will only take the 8bits from MSB
	.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
	.communication_format = I2S_COMM_FORMAT_I2S_MSB,
	.intr_alloc_flags = 0,							// default interrupt priority
	.dma_buf_count = 8,
	.dma_buf_len = 64,
	.use_apll = false
};

//esp_bd_addr_t lastAddr;


///////////////////////////////////////////////////////////////////////////////
// Functions

void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
	// calculate duty, 8191 from 2 ^ 13 - 1
	uint32_t duty = (8191 / valueMax) * min(value, valueMax);

	// write duty to LEDC
	ledcWrite(channel, duty);
}

void write_bt_address(esp_bd_addr_t addr) {

	for (int i = 0; i < ESP_BD_ADDR_LEN; i++) {
		Serial.write(":");
		char str[3];
		sprintf(str, "%02X", (int)addr[i]);
		Serial.print(str);
	}
}

bool initI2S() {
	i2s_driver_install(i2s_port, &i2s_config, 0, NULL);
	i2s_set_pin(i2s_port, NULL);
	i2s_set_dac_mode(I2S_DAC_CHANNEL_BOTH_EN);

}

bool initBluetooth(const char* deviceName) {

	if (!btStart()) {
		Serial.println("Failed to initialize controller");
		return false;
	}

	if (esp_bluedroid_init() != ESP_OK) {
		Serial.println("Failed to initialize bluedroid");
		return false;
	}

	if (esp_bluedroid_enable() != ESP_OK) {
		Serial.println("Failed to enable bluedroid");
		return false;
	}

	esp_a2d_register_callback(cb_a2d);
	esp_a2d_sink_register_data_callback(cb_audiodata);

	if (esp_a2d_sink_init() != ESP_OK) {
		Serial.println("Failed to enable a2dp");
		return false;
	}

	if (esp_avrc_ct_init() != ESP_OK) {
		Serial.println("Failed to enable AVRC");
		return false;
	}

	esp_avrc_ct_register_callback(cb_avrc_ct);

	esp_bt_dev_set_device_name(deviceName);
	esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);

	return true;
}

bool sendMediaCmd(uint8_t cmd) {

	// press button
	esp_err_t eval = esp_avrc_ct_send_passthrough_cmd(0, cmd, ESP_AVRC_PT_CMD_STATE_PRESSED);

	if (eval == ESP_ERR_INVALID_STATE) {
		Serial.println("ERROR: send Media Cmd press: BT stack not ready");

	}
	else if (eval == ESP_ERR_INVALID_STATE) {
		Serial.println("ERROR: Send Media Cmd press: Something else went wrong");
	}

	// wait
	delay(100);

	// release the button
	eval = esp_avrc_ct_send_passthrough_cmd(1, cmd, ESP_AVRC_PT_CMD_STATE_RELEASED);

	if (eval == ESP_ERR_INVALID_STATE) {
		Serial.println("ERROR: send Media Cmd release: BT stack not ready");

	}
	else if (eval == ESP_ERR_INVALID_STATE) {
		Serial.println("ERROR: send Media Cmd release: Something else went wrong");
	}

}

void reconnect() {
	size_t macLen = prefs.getBytesLength("mac");
	char buffer[macLen];
	if (prefs.getBytes("mac", buffer, macLen) != 0) {
		esp_bd_addr_t* newAddr = (esp_bd_addr_t*)buffer;
		Serial.write("Try to connect to ");
		write_bt_address(*newAddr);
		Serial.write("\n");
		esp_a2d_sink_connect(*newAddr);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Callbacks

void cb_a2d(esp_a2d_cb_event_t event, esp_a2d_cb_param_t* param) {

	switch (event) {

		// Connection /////////////////////////////////////////////////////////////
	case ESP_A2D_CONNECTION_STATE_EVT:
		Serial.write("A2D connection state changed to ");

		if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_DISCONNECTED) {
			Serial.write("DISCONNECTED\n");

		}
		else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_CONNECTING) {
			Serial.write("CONNECTING\n");

		}
		else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_CONNECTED) {
			Serial.write("CONNECTED\nDevice:");
			write_bt_address(param->conn_stat.remote_bda);
			Serial.write("\n");
			prefs.remove("mac");
			prefs.putBytes("mac", param->conn_stat.remote_bda, sizeof(param->conn_stat.remote_bda));

		}
		else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_DISCONNECTING) {
			Serial.write("DISCONNECTING\n");
		}

		break;

		// Audio //////////////////////////////////////////////////////////////////
	case ESP_A2D_AUDIO_STATE_EVT:
		Serial.write("audio state changed to ");

		if (param->audio_stat.state == ESP_A2D_AUDIO_STATE_STARTED) {
			Serial.write("STARTED\n");

		}
		else if (param->audio_stat.state == ESP_A2D_AUDIO_STATE_STOPPED) {
			Serial.write("STOPPED\n");

		}
		else if (param->audio_stat.state == ESP_A2D_AUDIO_STATE_REMOTE_SUSPEND) {
			Serial.write("SUSPENDED\n");
		}

		break;

		// Audio Codec Information ////////////////////////////////////////////////
	case ESP_A2D_AUDIO_CFG_EVT:
		Serial.println("audio codec is configured");
		Serial.write("type: ");

		if (param->audio_cfg.mcc.type == ESP_A2D_MCT_SBC) {
			Serial.write("SBC");

			i2s_config.sample_rate = 16000;
			char oct0 = param->audio_cfg.mcc.cie.sbc[0];

			if (oct0 & (0x01 << 6)) {
				i2s_config.sample_rate = 32000;

			}
			else if (oct0 & (0x01 << 5)) {
				i2s_config.sample_rate = 44100;

			}
			else if (oct0 & (0x01 << 4)) {
				i2s_config.sample_rate = 48000;
			}

			i2s_set_clk(i2s_port, i2s_config.sample_rate, i2s_config.bits_per_sample, (i2s_channel_t)2);

		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_M12) {
			Serial.write("MPEG-1, 2 Audio");

		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_M24) {
			Serial.write("MPEG-2, 4 AAC");

		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_ATRAC) {
			Serial.write("ATRAC family");

		}
		else {
			Serial.write("unknown ");
			Serial.write(param->audio_cfg.mcc.type);
		}

		Serial.write("\n");
		break;

		// Media Command Ack Signal ///////////////////////////////////////////////
	case ESP_A2D_MEDIA_CTRL_ACK_EVT:
		Serial.println("media command ack");
		break;

		// Sonstiges //////////////////////////////////////////////////////////////
	default:
		Serial.write("unknown value for esp_a2d_cb_event_t ");
		Serial.write(event);
		Serial.write("\n");
		break;
	}

}

void cb_audiodata(const uint8_t* data, uint32_t len) {

	size_t i2s_bytes_written;

	if (i2s_write(i2s_port, (void*)data, len, &i2s_bytes_written, portMAX_DELAY) != ESP_OK) {
		Serial.println("i2s_write has failed");
	}

	if (i2s_bytes_written < len) {
		Serial.println("Timeout: not all bytes were written to I2S");
	}

}

void cb_avrc_ct(esp_avrc_ct_cb_event_t event, esp_avrc_ct_cb_param_t* param) {

	switch (event) {

		// Connection /////////////////////////////////////////////////////////////
	case ESP_AVRC_CT_CONNECTION_STATE_EVT:
		Serial.write("AVRC connection state changed to ");

		if (param->conn_stat.connected == true) {
			Serial.write("CONNECTED\n");
		}
		else {
			Serial.write("DISCONNECTED\n");
		}

		break;

		// Passthrough Antwort ////////////////////////////////////////////////////
	case ESP_AVRC_CT_PASSTHROUGH_RSP_EVT:
		Serial.println("passthrough response");
		break;

		// Metadaten Antwort //////////////////////////////////////////////////////
	case ESP_AVRC_CT_METADATA_RSP_EVT:
		Serial.println("metadate response");
		break;

		// Playstatus Antwort /////////////////////////////////////////////////////
	case ESP_AVRC_CT_PLAY_STATUS_RSP_EVT:
		Serial.println("play status response");
		break;

		// Benachrichtigung ///////////////////////////////////////////////////////
	case ESP_AVRC_CT_CHANGE_NOTIFY_EVT:
		Serial.println("notification from avrct");
		break;

		// Feature Information ////////////////////////////////////////////////////
	case ESP_AVRC_CT_REMOTE_FEATURES_EVT:
		Serial.println("feature of remote device indication");
		break;

		// Sonstiges //////////////////////////////////////////////////////////////
	default:
		Serial.write("unknown value for esp_avrc_ct_cb_event_t ");
		Serial.write(event);
		Serial.write("\n");
		break;
	}

}


///////////////////////////////////////////////////////////////////////////////
// Default

void setup() {

	prefs.begin("mac");

	Serial.begin(115200);

	if (!initBluetooth("Mercedes Benz W208")) {
		Serial.println("Bluetooth init failed");

	}
	else {
		Serial.println("Bluetooth init success");
	}

	initI2S();

	reconnect();

	ledcSetup(LEDC_CHANNEL_0, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(LED_PIN, LEDC_CHANNEL_0);

	ledcAnalogWrite(LEDC_CHANNEL_0, 255);
}

void loop() {

	if (Serial.available()) {
		cCurrentChar = Serial.read();

		if (cCurrentChar != '\n') {
			sMessage += String(cCurrentChar);

		}
		else {
			sMessage = "";
		}

		if (sMessage == "play") {
			sendMediaCmd(ESP_AVRC_PT_CMD_PLAY);

		}
		else if (sMessage == "pause") {
			sendMediaCmd(ESP_AVRC_PT_CMD_PAUSE);

		}
		else if (sMessage == "next") {
			sendMediaCmd(ESP_AVRC_PT_CMD_FORWARD);

		}
		else if (sMessage == "prev") {
			sendMediaCmd(ESP_AVRC_PT_CMD_BACKWARD);

		}
		else if (sMessage == "connect") {
			reconnect();
		}
		else if (sMessage == "0") {
			ledcAnalogWrite(LEDC_CHANNEL_0, 0);
		}
		else if (sMessage == "50") {
			ledcAnalogWrite(LEDC_CHANNEL_0, 50);
		}
		else if (sMessage == "100") {
			ledcAnalogWrite(LEDC_CHANNEL_0, 100);
		}
		else if (sMessage == "255") {
			ledcAnalogWrite(LEDC_CHANNEL_0, 255);
		}

	}

	//int outputValue = (sin((double)iCurStep * (2.0 * PI) / (double)iSteps) * 120.0) + 135.0;
	//iCurStep++;
	//if (iCurStep >= iSteps){
	 // iCurStep = 0;
	//}
	//ledcAnalogWrite(LEDC_CHANNEL_0,outputValue);
	  //delay(1000.0 * (1/(double)iFreq)/(double)iSteps);  
	delay(20);
}
