#include <BleKeyboard.h>

#define LED_EXTERN 23

BleKeyboard bleKeyboard;

char c_currentChar;
String s_message;

void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");
  bleKeyboard.begin();

  pinMode(LED_EXTERN, OUTPUT);
  digitalWrite(LED_EXTERN, LOW);
}

void loop() {
  if (Serial.available()) {
    
    c_currentChar = Serial.read();
    if (c_currentChar != '\n') {
      s_message += String(c_currentChar);
    }
    else {
      s_message = "";
    }

    if (s_message == "on") {
      digitalWrite(LED_EXTERN, HIGH);
    }
    else if (s_message == "off") {
      digitalWrite(LED_EXTERN, LOW);
    }
    else if (s_message == "hall") {
      Serial.print(hallRead());
    }
    else if(bleKeyboard.isConnected()){
    
      if (s_message == "play") {
        bleKeyboard.write(KEY_MEDIA_PLAY_PAUSE);
      }
      else if (s_message == "next") {
        bleKeyboard.write(KEY_MEDIA_NEXT_TRACK);
      }
      else if (s_message == "prev") {
        bleKeyboard.write(KEY_MEDIA_PREVIOUS_TRACK);
      }
    }
  }
  delay(20);
}
