
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
  #error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#define LED_BUILTIN 1
#define LED_EXTERN 23

BluetoothSerial SerialBT;

int i_ledState = LOW;
char c_currentChar;
String s_message;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32test");
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  pinMode(LED_EXTERN, OUTPUT);
  digitalWrite(LED_EXTERN, LOW);
}

// the loop function runs over and over again forever
void loop() { 
  
  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    c_currentChar = SerialBT.read();
    if (c_currentChar != '\n') {
      s_message += String(c_currentChar);
    }
    else {
      s_message = "";
    }

    if (s_message == "on") {
      digitalWrite(LED_EXTERN, HIGH);
    }
    else if (s_message == "off") {
      digitalWrite(LED_EXTERN, LOW);
    }
    else if (s_message == "hall") {
      SerialBT.print(hallRead());
    }
  }
  delay(20);
}
