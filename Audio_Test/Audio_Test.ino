/*
 Name:		Audio_Test.ino
 Created:	22.11.2020 10:31:36
 Author:	
*/

#include <driver/dac.h>
#include <driver/i2s.h>
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"
#include "esp_a2dp_api.h"
#include "esp_avrc_api.h"

#define PIN_MUTE 14
#define PIN_WSEL 25
#define PIN_DIN 26
#define PIN_BCLK 27


String sMessage;

int amplitude = 100;
int freq = 10;

i2s_config_t i2s_config = {
	.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX ),
	.sample_rate = 48000, 							// corrected by info from bluetooth
	.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT, 	//the DAC module will only take the 8bits from MSB
	.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
	.communication_format = I2S_COMM_FORMAT_I2S,
	.intr_alloc_flags = 0,							// default interrupt priority
	.dma_buf_count = 8,
	.dma_buf_len = 64,
	.use_apll = false
};

static const i2s_pin_config_t pin_config = {
  .bck_io_num = PIN_BCLK,
  .ws_io_num = PIN_WSEL,
  .data_out_num = PIN_DIN,
	.data_in_num = I2S_PIN_NO_CHANGE
};

void cb_a2d(esp_a2d_cb_event_t event, esp_a2d_cb_param_t* param) {

	if (event == ESP_A2D_AUDIO_CFG_EVT) {
		Serial.println("audio codec config");
		Serial.write("type: ");

		if (param->audio_cfg.mcc.type == ESP_A2D_MCT_SBC) {
			Serial.write("SBC");

			i2s_config.sample_rate = 16000;
			char oct0 = param->audio_cfg.mcc.cie.sbc[0];

			if (oct0 & (0x01 << 6))
				i2s_config.sample_rate = 32000;
			else if (oct0 & (0x01 << 5))
				i2s_config.sample_rate = 44100;
			else if (oct0 & (0x01 << 4))
				i2s_config.sample_rate = 48000;

			Serial.println(i2s_config.sample_rate);

			//i2s_set_clk(I2S_NUM_0, i2s_config.sample_rate, i2s_config.bits_per_sample,  I2S_CHANNEL_STEREO);
			i2s_set_sample_rates(I2S_NUM_0, i2s_config.sample_rate);
		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_M12) {
			Serial.write("MPEG-1, 2 Audio");
		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_M24) {
			Serial.write("MPEG-2, 4 AAC");
		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_ATRAC) {
			Serial.write("ATRAC family");
		}
		else {
			Serial.write("unknown ");
			Serial.write(param->audio_cfg.mcc.type);
		}

		Serial.write("\n");
	}
}

void cb_audiodata(const uint8_t* data, uint32_t len) {

	size_t i2s_bytes_written;

	if (i2s_write(I2S_NUM_0, (void*)data, len, &i2s_bytes_written, portMAX_DELAY) != ESP_OK)
		Serial.println("i2s_write has failed");

	if (i2s_bytes_written < len)
		Serial.println("Timeout: not all bytes were written to I2S");
}

bool bt_init(const char* deviceName, esp_a2d_cb_t cbA2DP, esp_a2d_sink_data_cb_t cbA2DPdata)
{
	if (!btStart()) {
		Serial.println("Failed to start bt");
		return false;
	}

	if (esp_bluedroid_init() != ESP_OK) {
		Serial.println("Failed to initialize bluedroid");
		return false;
	}

	if (esp_bluedroid_enable() != ESP_OK) {
		Serial.println("Failed to enable bluedroid");
		return false;
	}

	esp_a2d_register_callback(cbA2DP);
	esp_a2d_sink_register_data_callback(cbA2DPdata);

	if (esp_a2d_sink_init() != ESP_OK) {
		Serial.println("Failed to enable a2dp");
		return false;
	}

	esp_bt_dev_set_device_name(deviceName);
	esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);

	return true;
}

void setup_sin() {

	dac_output_enable(DAC_CHANNEL_1);
	dac_output_voltage(DAC_CHANNEL_1, 0);
}

void setup_bti2s() {

	bt_init("ESP32 Audiotest", cb_a2d, cb_audiodata);

	i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);
	i2s_set_pin(I2S_NUM_0, &pin_config);
	//i2s_set_pin(I2S_NUM_0, NULL);
	//i2s_set_dac_mode(I2S_DAC_CHANNEL_BOTH_EN);

	i2s_set_sample_rates(I2S_NUM_0, i2s_config.sample_rate);

}

void setup() {
	Serial.begin(115200); delay(1000);
  pinMode(PIN_MUTE, OUTPUT);
  digitalWrite(PIN_MUTE, LOW);
	//setup_sin();
	setup_bti2s();
}

void loop_sin() {

	int voltage = (sin(2 * PI * freq * millis() / 1000) + 1) * amplitude;

	Serial.print(freq);
	Serial.print(" ");
	Serial.print(amplitude);
	Serial.print(" ");
	Serial.println(voltage);

	dac_output_voltage(DAC_CHANNEL_1, voltage);

	if (Serial.available()) {
		char cInput = Serial.read();

		if (cInput != '\n')
			sMessage += String(cInput);
		else {
			freq = sMessage.toInt();
			sMessage = "";
		}
    
		if (sMessage == "mute") {
		}
		else if (sMessage == "play") {
		}
	}
}


// the loop function runs over and over again until power down or reset
void loop() {
  if (Serial.available()) {
    char cInput = Serial.read();

    if (cInput != '\n')
      sMessage += String(cInput);
    else {
      //freq = sMessage.toInt();
      sMessage = "";
    }
    Serial.println(sMessage);
    if (sMessage == "mute") {
      Serial.println("mute");
      digitalWrite(PIN_MUTE, HIGH);
    }
    else if (sMessage == "play") {
      Serial.println("play");
      digitalWrite(PIN_MUTE, LOW);
    }
  }
	//loop_sin();
}
