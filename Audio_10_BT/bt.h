// bt.h

#ifndef _BT_h
#define _BT_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"
#include "esp_a2dp_api.h"
#include "esp_avrc_api.h"

bool bt_init(const char* deviceName, esp_a2d_cb_t cbA2DP, esp_a2d_sink_data_cb_t cbA2DPdata, esp_avrc_ct_cb_t cbAVRC);
void bt_deinit();

void bt_write_address(esp_bd_addr_t addr);

#endif

