// 
// 
// 

#include "measurements.h"

void catch_and_print_states() {
	int state;

	state = digitalRead(mPIN_CCEJT);
	if (state != iCCEJTstate) {
		iCCEJTstate = state;
		Serial.print(millis());
		Serial.write(" CCEJT: ");
		Serial.println(state);
	}

	/*state = digitalRead(mPIN_CCLDG);
	if (state != iCCLDGstate) {
		iCCLDGstate = state;
		Serial.print(millis());
		Serial.write(" CCLDG: ");
		Serial.println(state);
	}*/

	state = digitalRead(mPIN_LED);
	if (state != iCCMOTstate) {
		iCCMOTstate = state;
		Serial.print(millis());
		Serial.write(" CCMOT: ");
		Serial.println(state);
	}
}

void setup_operation_measure() {
	Serial.begin(115200); delay(1000);
	Serial.println("timestamp;CCEJCT;CCLDG;CCMOD;CCPSE;");

	//pinMode(mPIN_CCMET, INPUT);
	//pinMode(mPIN_CCINS, INPUT);
	pinMode(mPIN_CCEJT, INPUT);
	pinMode(mPIN_CCMOD, INPUT);
	pinMode(mPIN_CCPSE, INPUT);

	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);		//CCLDG
}

void setup_freq_measure() {
	Serial.begin(115200); delay(1000);
	Serial.println("timestamp;REEL_R;REEL_F;");

	pinMode(mPIN_REEL_R, INPUT);
	pinMode(mPIN_REEL_F, INPUT);
}

void loop_freq_measure() {

	Serial.print(millis());
	Serial.write(";");
	Serial.print(digitalRead(mPIN_REEL_R));
	Serial.write(";");
	Serial.print(digitalRead(mPIN_REEL_F));
	Serial.println(";");

	delay(20);
}

void loop_operation_measure() {

	Serial.print(millis());
	Serial.write(";");
	//Serial.print(digitalRead(mPIN_CCMET));
	//Serial.write(";");
	//Serial.print(digitalRead(mPIN_CCINS));
	//Serial.write(";");
	Serial.print(digitalRead(mPIN_CCEJT));
	Serial.write(";");
	Serial.print(adc1_get_raw(ADC1_CHANNEL_6));
	Serial.write(";");
	Serial.print(digitalRead(mPIN_CCMOD));
	Serial.write(";");
	Serial.print(digitalRead(mPIN_CCPSE));
	Serial.println(";");

	//delay(20);
}
