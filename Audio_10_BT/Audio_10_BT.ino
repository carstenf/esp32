/*
 Name:		Audio_10_BT.ino
 Created:	24.10.2020 14:58:58
 Author:	Carsten Fuhrmann

 INFORMATION SECTION

 Play Opto Speed
 200 ms up/down time => 2,5Hz

 FF Opto Speed
 100ms up/down time => 5Hz

 REV Optp Speed
 50ms up/down time => 10Hz

*/


///////////////////////////////////////////////////////////////////////////////
// INCLUDES & DEFINES

//#include "src/header/measurements.h"
#include <driver/i2s.h>
#include "bt.h"

#define PIN_CCINS 2
#define PIN_CCEJT 4
#define PIN_CCLDG 5
#define PIN_OPTO 21
#define PIN_CCMOD 18
#define PIN_CCPSE 22
#define PIN_LED 23
#define PIN_WAKEUP GPIO_NUM_15
#define PIN_MUTE 14
#define PIN_WSEL 25
#define PIN_DIN 26
#define PIN_BCLK 27

#define TRIGGER_GND HIGH
#define TRIGGER_5V LOW
#define OPTO_PERIOD 200


///////////////////////////////////////////////////////////////////////////////
// Variables

typedef enum {
	standby = 0,
	forward = 1,
	reverse = 2,
	brake = 3,
	err = 4
} DriveState;

typedef enum {
	empty = 0,
	connected = 1,
	idle = 2,
	is1 = 3,
	rev = 4,
	is2 = 5,
	ff = 6,
	is3 = 7,
	play1 = 8,
	is4 = 9,
	play2 = 10
} GearState;

RTC_DATA_ATTR struct States {
	esp_bd_addr_t lastDevice;
	int gear;
	int gearTime;
	int operation = DriveState::standby;
	int optoTime = 2 * OPTO_PERIOD;
	unsigned long timeStamp = 0;
	unsigned long brakeTime = 0;
} ccState;

i2s_config_t i2s_config = {
	.mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX ),
	.sample_rate = 48000, 							// corrected by info from bluetooth
	.bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT, 	//the DAC module will only take the 8bits from MSB
	.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
	.communication_format = I2S_COMM_FORMAT_I2S,
	.intr_alloc_flags = 0,							// default interrupt priority
	.dma_buf_count = 8,
	.dma_buf_len = 64,
	.use_apll = false
};

static const i2s_pin_config_t pin_config = {
	.bck_io_num = PIN_BCLK,
	.ws_io_num = PIN_WSEL,
	.data_out_num = PIN_DIN,
	.data_in_num = I2S_PIN_NO_CHANGE
};

bool blockOpto = true;

String sMessage;

///////////////////////////////////////////////////////////////////////////////
// Callbacks

void cb_a2d(esp_a2d_cb_event_t event, esp_a2d_cb_param_t* param) {

	switch (event) {

		// Connection /////////////////////////////////////////////////////////////
	case ESP_A2D_CONNECTION_STATE_EVT:
		Serial.write("A2D connection state changed to ");

		if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_DISCONNECTED) {
			Serial.write("DISCONNECTED\n");
			digitalWrite(PIN_CCINS, TRIGGER_GND);
			digitalWrite(PIN_CCPSE, TRIGGER_GND);
			digitalWrite(PIN_CCMOD, TRIGGER_5V);
			blockOpto = true;
			deinit_i2s();
		}
		else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_CONNECTING) {
			Serial.write("CONNECTING\n");
			init_i2s();
		}
		else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_CONNECTED) {
			Serial.write("CONNECTED\nDevice:");
			bt_write_address(param->conn_stat.remote_bda);
			Serial.write("\n");
			
			blockOpto = false;

			for (int i = 0; i < ESP_BD_ADDR_LEN; i++)
				ccState.lastDevice[i] = param->conn_stat.remote_bda[i];
			digitalWrite(PIN_CCINS, TRIGGER_5V);
		}
		else if (param->conn_stat.state == ESP_A2D_CONNECTION_STATE_DISCONNECTING) {
			Serial.write("DISCONNECTING\n");
		}

		break;

		// Audio //////////////////////////////////////////////////////////////////
	case ESP_A2D_AUDIO_STATE_EVT:
		Serial.write("audio state changed to ");

		if (param->audio_stat.state == ESP_A2D_AUDIO_STATE_STARTED) {
			Serial.write("STARTED\n");
		}
		else if (param->audio_stat.state == ESP_A2D_AUDIO_STATE_STOPPED) {
			Serial.write("STOPPED\n");
		}
		else if (param->audio_stat.state == ESP_A2D_AUDIO_STATE_REMOTE_SUSPEND) {
			Serial.write("SUSPENDED\n");
		}

		break;

		// Audio Codec Information ////////////////////////////////////////////////
	case ESP_A2D_AUDIO_CFG_EVT:
		Serial.println("audio codec is configured");
		Serial.write("type: ");

		if (param->audio_cfg.mcc.type == ESP_A2D_MCT_SBC) {
			Serial.write("SBC");

			i2s_config.sample_rate = 16000;
			char oct0 = param->audio_cfg.mcc.cie.sbc[0];

			if (oct0 & (0x01 << 6))
				i2s_config.sample_rate = 32000;
			else if (oct0 & (0x01 << 5))
				i2s_config.sample_rate = 44100;
			else if (oct0 & (0x01 << 4))
				i2s_config.sample_rate = 48000;

			//i2s_set_clk(I2S_NUM_0, i2s_config.sample_rate, i2s_config.bits_per_sample, (i2s_channel_t)2);
      i2s_set_sample_rates(I2S_NUM_0, i2s_config.sample_rate);
		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_M12) {
			Serial.write("MPEG-1, 2 Audio");
		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_M24) {
			Serial.write("MPEG-2, 4 AAC");
		}
		else if (param->audio_cfg.mcc.type == ESP_A2D_MCT_ATRAC) {
			Serial.write("ATRAC family");
		}
		else {
			Serial.write("unknown ");
			Serial.write(param->audio_cfg.mcc.type);
		}

		Serial.write("\n");
		break;

		// Media Command Ack Signal ///////////////////////////////////////////////
	case ESP_A2D_MEDIA_CTRL_ACK_EVT:
		Serial.println("media command ack");
		break;

		// Sonstiges //////////////////////////////////////////////////////////////
	default:
		Serial.write("unknown value for esp_a2d_cb_event_t ");
		Serial.write(event);
		Serial.write("\n");
		break;
	}
}

void cb_audiodata(const uint8_t* data, uint32_t len) {

	size_t i2s_bytes_written;

	if (i2s_write(I2S_NUM_0, (void*)data, len, &i2s_bytes_written, portMAX_DELAY) != ESP_OK)
		Serial.println("i2s_write has failed");

	if (i2s_bytes_written < len)
		Serial.println("Timeout: not all bytes were written to I2S");
}

void cb_avrc_ct(esp_avrc_ct_cb_event_t event, esp_avrc_ct_cb_param_t* param) {

	switch (event) {

		// Connection /////////////////////////////////////////////////////////////
	case ESP_AVRC_CT_CONNECTION_STATE_EVT:
		Serial.write("AVRC connection state changed to ");

		if (param->conn_stat.connected == true) {
			Serial.write("CONNECTED\n");
		}
		else {
			Serial.write("DISCONNECTED\n");
		}

		break;

		// Passthrough Antwort ////////////////////////////////////////////////////
	case ESP_AVRC_CT_PASSTHROUGH_RSP_EVT:
		Serial.println("passthrough response");
		break;

		// Metadaten Antwort //////////////////////////////////////////////////////
	case ESP_AVRC_CT_METADATA_RSP_EVT:
		Serial.println("metadate response");
		break;

		// Playstatus Antwort /////////////////////////////////////////////////////
	case ESP_AVRC_CT_PLAY_STATUS_RSP_EVT:
		Serial.println("play status response");
		break;

		// Benachrichtigung ///////////////////////////////////////////////////////
	case ESP_AVRC_CT_CHANGE_NOTIFY_EVT:
		Serial.println("notification from avrct");
		break;

		// Feature Information ////////////////////////////////////////////////////
	case ESP_AVRC_CT_REMOTE_FEATURES_EVT:
		Serial.println("feature of remote device indication");
		break;

		// Sonstiges //////////////////////////////////////////////////////////////
	default:
		Serial.write("unknown value for esp_avrc_ct_cb_event_t ");
		Serial.write(event);
		Serial.write("\n");
		break;
	}

}


///////////////////////////////////////////////////////////////////////////////
// Functions

void init_i2s() {
	Serial.println("init i2s");
	i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);
  i2s_set_pin(I2S_NUM_0, &pin_config);
	//i2s_set_pin(I2S_NUM_0, NULL);
	//i2s_set_dac_mode(I2S_DAC_CHANNEL_BOTH_EN);
}

void deinit_i2s() {
	Serial.println("deinit i2s");
	i2s_driver_uninstall(I2S_NUM_0);
}

void go_to_sleep() {

	Serial.println("good night");
	if (ccState.gear == GearState::empty)
		Serial.println("is empty");
	else if (ccState.gear == GearState::connected)
		Serial.println("is connected");
	else
		Serial.println("is something else");
	/*if (digitalRead(PIN_CCINS) == 1)
		esp_a2d_sink_disconnect(ccState.lastDevice);*/

	//delay(2000);

	//bt_deinit();

	//deinit i2s

	Serial.flush();
	Serial.end();

	esp_sleep_enable_ext0_wakeup(PIN_WAKEUP, HIGH);
	esp_deep_sleep_start();
}

void reconnect() {

	if (ccState.lastDevice[0] == 0 &&
		ccState.lastDevice[1] == 0 &&
		ccState.lastDevice[2] == 0 &&
		ccState.lastDevice[3] == 0 &&
		ccState.lastDevice[4] == 0)
		return;

	Serial.write("trying to connect to ");
	bt_write_address(ccState.lastDevice);
	Serial.write("\n");
	esp_a2d_sink_connect(ccState.lastDevice);
}

bool send_media_cmd(uint8_t cmd) {

	// press button
	esp_err_t eval = esp_avrc_ct_send_passthrough_cmd(0, cmd, ESP_AVRC_PT_CMD_STATE_PRESSED);

	if (eval == ESP_ERR_INVALID_STATE)
		Serial.println("ERROR: send Media Cmd press: BT stack not ready");
	else if (eval == ESP_ERR_INVALID_STATE)
		Serial.println("ERROR: Send Media Cmd press: Something else went wrong");

	// wait
	delay(100);

	// release the button
	eval = esp_avrc_ct_send_passthrough_cmd(1, cmd, ESP_AVRC_PT_CMD_STATE_RELEASED);

	if (eval == ESP_ERR_INVALID_STATE)
		Serial.println("ERROR: send Media Cmd release: BT stack not ready");
	else if (eval == ESP_ERR_INVALID_STATE)
		Serial.println("ERROR: send Media Cmd release: Something else went wrong");

}

void new_gear_state() {

	// make sure it is a new brake operation
	if (ccState.timeStamp > (ccState.brakeTime + 200)) {
		ccState.brakeTime = ccState.timeStamp;

		Serial.println("gear stopped");

		switch (ccState.gear)
		{
		case GearState::empty:
			esp_a2d_sink_disconnect(ccState.lastDevice);
			digitalWrite(PIN_MUTE, HIGH);
			ccState.gearTime = 50;
			break;
		case GearState::connected:
			send_media_cmd(ESP_AVRC_PT_CMD_PAUSE);
			digitalWrite(PIN_MUTE, HIGH);
			ccState.gearTime = 150;
			break;
		case GearState::play2:
			send_media_cmd(ESP_AVRC_PT_CMD_PAUSE);
			digitalWrite(PIN_MUTE, HIGH);
			ccState.gearTime = 952;
			break;
		case GearState::idle:
			ccState.gearTime = 220;
			break;
		case GearState::rev:
			send_media_cmd(ESP_AVRC_PT_CMD_BACKWARD);
			ccState.gearTime = 352;
			break;
		case GearState::ff:
			send_media_cmd(ESP_AVRC_PT_CMD_FORWARD);
			ccState.gearTime = 592;
			break;
		case GearState::play1:
			send_media_cmd(ESP_AVRC_PT_CMD_PLAY);
			digitalWrite(PIN_MUTE, LOW);
			ccState.gearTime = 782;
			break;
		default:
			Serial.println("Warning: gear stopped at unexpected position.");
			ccState.gearTime = 50;
			ccState.gear = GearState::empty;
			digitalWrite(PIN_CCINS, TRIGGER_GND);
			digitalWrite(PIN_CCPSE, TRIGGER_GND);
			digitalWrite(PIN_MUTE, HIGH);
			digitalWrite(PIN_CCMOD, TRIGGER_5V);
			break;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Setup

void setup_pin_mods() {

	pinMode(PIN_CCPSE, OUTPUT);
	pinMode(PIN_CCMOD, OUTPUT);
	pinMode(PIN_CCINS, OUTPUT);
	pinMode(PIN_OPTO, OUTPUT);
	pinMode(PIN_MUTE, OUTPUT);

	pinMode(PIN_CCEJT, INPUT);
	pinMode(PIN_LED, INPUT);
	pinMode(PIN_CCLDG, INPUT);
	pinMode(PIN_WAKEUP, INPUT);
}

void setup_default_states() {

	// It should always wake up empty or connected
	if (ccState.gear != GearState::empty && ccState.gear != GearState::connected) {
		ccState.gear = GearState::empty;
		ccState.gearTime = 0;
	}

	ccState.optoTime = DriveState::standby;
	ccState.optoTime = 2 * OPTO_PERIOD;
	ccState.timeStamp = 0;
	ccState.brakeTime = 0;

	digitalWrite(PIN_CCPSE, TRIGGER_GND);
	digitalWrite(PIN_CCMOD, TRIGGER_5V);
	digitalWrite(PIN_MUTE, HIGH);
	if (ccState.gear == GearState::connected)
		digitalWrite(PIN_CCINS, TRIGGER_5V);
	else
		digitalWrite(PIN_CCINS, TRIGGER_GND);
}


void setup() {
	Serial.begin(115200); delay(1000);
	Serial.println("MB Audio 10 BT start");

	if (ccState.gear == GearState::empty)
		Serial.println("was empty");
	else if (ccState.gear == GearState::connected)
		Serial.println("was connected");
	else
		Serial.println("was something else");

	setup_pin_mods();
	setup_default_states();

	if (!bt_init("Mercedes Audio 10 BT", cb_a2d, cb_audiodata, cb_avrc_ct)) {
		Serial.println("bt init failed, restart in 1 sec");
		delay(1000);
		ESP.restart();
	}

	//if (ccState.gear == GearState::connected)
	reconnect();

	ccState.timeStamp = millis();

	Serial.println("setup done");
}


///////////////////////////////////////////////////////////////////////////////
// loop

void loop_emulation() {

	// get current states ////////////////////////////////////////////////////////

	int readLDG = digitalRead(PIN_CCLDG);
	int readEJT = digitalRead(PIN_CCEJT);
	int readLED = digitalRead(PIN_LED);

	if (readLDG == 1 && readEJT == 1)
		ccState.operation = DriveState::brake;
	else if (readLDG == 1 && readEJT == 0)
		ccState.operation = DriveState::forward;
	else if (readLDG == 0 && readEJT == 1)
		ccState.operation = DriveState::reverse;
	else if (readLDG == 0 && readEJT == 0)
		ccState.operation = DriveState::standby;
	else
		ccState.operation = DriveState::err;

	int timeDiff = millis() - ccState.timeStamp;
	ccState.timeStamp = millis();


	// optical sensor ////////////////////////////////////////////////////////////

	if (readLED == HIGH && blockOpto != true) {
		ccState.optoTime += timeDiff;
		if (ccState.optoTime >= OPTO_PERIOD) {
			digitalWrite(PIN_OPTO, !digitalRead(PIN_OPTO));
			ccState.optoTime = 0;
		}
	}
	else if (readLED == LOW && ccState.optoTime != 2 * OPTO_PERIOD) {
		//Serial.println("end PWM signal");
		digitalWrite(PIN_OPTO, LOW);
		ccState.optoTime = 2 * OPTO_PERIOD;
	}


	// gear emulation ////////////////////////////////////////////////////////////

	switch (ccState.operation)
	{
	case DriveState::standby:
		break;
	case DriveState::forward:
		ccState.gearTime += timeDiff;
		break;
	case DriveState::reverse:
		ccState.gearTime -= timeDiff;
		break;
	case DriveState::brake:
		new_gear_state();
		break;
	default:
		Serial.println("ERROR: Unknown Drivestate");
		break;
	}

	// empty
	if (ccState.gearTime < 100 && ccState.gear != GearState::empty) {
		digitalWrite(PIN_CCINS, TRIGGER_GND);

		//Serial.println("Gear: empty");

		ccState.gear = GearState::empty;
	}
	//connected
	else if (100 <= ccState.gearTime && ccState.gearTime < 200 && ccState.gear != GearState::connected) {
		if (ccState.gear == GearState::idle)
			digitalWrite(PIN_CCPSE, TRIGGER_GND);

		//Serial.println("Gear: connected");

		ccState.gear = GearState::connected;
	}
	//idle
	else if (200 <= ccState.gearTime && ccState.gearTime < 240 && ccState.gear != GearState::idle) {
		if (ccState.gear == GearState::connected)
			digitalWrite(PIN_CCPSE, TRIGGER_5V);
		else if (ccState.gear == GearState::is1)
			digitalWrite(PIN_CCMOD, TRIGGER_5V);

		ccState.gear = GearState::idle;
	}
	//is1
	else if (240 <= ccState.gearTime && ccState.gearTime < 320 && ccState.gear != GearState::is1) {
		digitalWrite(PIN_CCMOD, TRIGGER_GND);
		ccState.gear = GearState::is1;
	}
	//rev
	else if (320 <= ccState.gearTime && ccState.gearTime < 385 && ccState.gear != GearState::rev) {
		digitalWrite(PIN_CCMOD, TRIGGER_5V);
		ccState.gear = GearState::rev;
	}
	//is2
	else if (385 <= ccState.gearTime && ccState.gearTime < 560 && ccState.gear != GearState::is2) {
		digitalWrite(PIN_CCMOD, TRIGGER_GND);
		ccState.gear = GearState::is2;
	}
	//ff
	else if (560 <= ccState.gearTime && ccState.gearTime < 625 && ccState.gear != GearState::ff) {
		digitalWrite(PIN_CCMOD, TRIGGER_5V);
		ccState.gear = GearState::ff;
	}
	//is3
	else if (625 <= ccState.gearTime && ccState.gearTime < 750 && ccState.gear != GearState::is3) {
		digitalWrite(PIN_CCMOD, TRIGGER_GND);
		ccState.gear = GearState::is3;
	}
	//play1
	else if (750 <= ccState.gearTime && ccState.gearTime < 815 && ccState.gear != GearState::play1) {
		digitalWrite(PIN_CCMOD, TRIGGER_5V);
		ccState.gear = GearState::play1;
	}
	//is4
	else if (815 <= ccState.gearTime && ccState.gearTime < 920 && ccState.gear != GearState::is4) {
		digitalWrite(PIN_CCMOD, TRIGGER_GND);
		ccState.gear = GearState::is4;
	}
	//play2
	else if (920 <= ccState.gearTime && ccState.gear != GearState::play2) {
		digitalWrite(PIN_CCMOD, TRIGGER_5V);
		ccState.gear = GearState::play2;
	}


	// delay /////////////////////////////////////////////////////////////////////

	delay(5);
}


void loop() {

	if (digitalRead(PIN_WAKEUP) == LOW)
		go_to_sleep();

	// delay 0
	//loop_input();

	// delay 5
	loop_emulation();

	return;
}
