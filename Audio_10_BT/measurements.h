// measurements.h

#ifndef _MEASUREMENTS_h
#define _MEASUREMENTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
	#include <driver/adc.h>
#else
	#include "WProgram.h"
#endif

const int mPIN_REEL_R = 18;
const int mPIN_REEL_F = 19;
const int mPIN_CCMET = 1;
const int mPIN_CCINS = 2;
const int mPIN_CCEJT = 25;
const int mPIN_CCMOD = 26;
const int mPIN_CCPSE = 27;
const int mPIN_LED = 2;
const int mPIN_CCLDG = 3;

int iCCEJTstate = 0;
int iCCMOTstate = 0;
int iCCLDGstate = 0;

void catch_and_print_states();
void setup_operation_measure();
void setup_freq_measure();
void loop_freq_measure();
void loop_operation_measure();

#endif

