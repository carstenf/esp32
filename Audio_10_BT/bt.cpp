// 
// 
// 

#include "bt.h"


bool bt_init(const char* deviceName, esp_a2d_cb_t cbA2DP, esp_a2d_sink_data_cb_t cbA2DPdata, esp_avrc_ct_cb_t cbAVRC)
{
	if (!btStart()) {
		Serial.println("Failed to start bt");
		return false;
	}

	if (esp_bluedroid_init() != ESP_OK) {
		Serial.println("Failed to initialize bluedroid");
		return false;
	}

	if (esp_bluedroid_enable() != ESP_OK) {
		Serial.println("Failed to enable bluedroid");
		return false;
	}

	esp_a2d_register_callback(cbA2DP);
	esp_a2d_sink_register_data_callback(cbA2DPdata);

	if (esp_a2d_sink_init() != ESP_OK) {
		Serial.println("Failed to enable a2dp");
		return false;
	}

	if (esp_avrc_ct_init() != ESP_OK) {
		Serial.println("Failed to enable AVRC");
		return false;
	}

	esp_avrc_ct_register_callback(cbAVRC);

	esp_bt_dev_set_device_name(deviceName);
	esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);

	return true;
}

void bt_deinit() {
	
	Serial.println("Deinit AVRC");
	if (esp_avrc_ct_deinit() != ESP_OK)
		Serial.write("Failed to deinit avrc");

	Serial.println("Deinit A2DP");
	if (esp_a2d_source_deinit() != ESP_OK)
		Serial.println("Failed to deinit a2dp");
	delay(5000);
	Serial.println("disable bluedroid");
	if (esp_bluedroid_disable() != ESP_OK)
		Serial.println("Failed to disable bluedroid");
	delay(5000);
	Serial.println("Deinit bluedroid");
	if (esp_bluedroid_deinit() != ESP_OK)
		Serial.println("Failed to deinit bluedroid");

	Serial.println("Stop BT");
	if (btStop() == false)
		Serial.println("Failed to stop bt");
}

void bt_write_address(esp_bd_addr_t addr)
{
	for (int i = 0; i < ESP_BD_ADDR_LEN; i++) {
		Serial.write(":");
		char str[3];
		sprintf(str, "%02X", (int)addr[i]);
		Serial.print(str);
	}
}
